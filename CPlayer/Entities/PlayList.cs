﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPlayer.Entities
{
    public class PlayList
    {
        public int Index { get; set; }
        public string FullPath { get; set; }
    }
}
