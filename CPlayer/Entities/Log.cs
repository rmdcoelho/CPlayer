﻿using System;
using System.Diagnostics;
using System.IO;

namespace CPlayer.Entities
{
    public class Log
    {
        const string LOG_NAME = "MeuLog"; //Nome da entrada no Visualizador de Eventos do Windows 
        const string SOURCE = "MinhaAplicacao"; //Nome da fonte (source) com que serão gravados os logs. 

        public Log()
        {
            //verifica se o log já existe, se não existe então cria;  
            //if (EventLog.SourceExists(SOURCE) == false)
            //    EventLog.CreateEventSource(SOURCE, LOG_NAME);
        }

        public void WriteEntry(string input, EventLogEntryType entryType)
        {
            //grava o texto na fonte de logs com o nome que      definimos para a constante SOURCE.  
            //EventLog.WriteEntry(SOURCE, input, entryType);
        }

        public void WriteEntry(string input)
        {
            //loga um simples evento com a categoria de informação.  
            //WriteEntry(input, EventLogEntryType.Information);
        }

        public void WriteEntry(Exception ex)
        {
            //loga a ocorrência de uma excessão com a categoria de erro.  
            //WriteEntry(ex.ToString(), EventLogEntryType.Error);
        }

        private string path = System.AppDomain.CurrentDomain.BaseDirectory.ToString();

        public void WriteErros(string msg, Exception ex)
        {
            var dataLog = DateTime.Now.ToString();
            var dataLogFormatada = dataLog.Replace(":", "").Replace(" ", "").Replace("/","");

            string path2 = path.Remove(path.LastIndexOf("\\"));
            path = path2.Remove(path2.LastIndexOf("\\") + 1);
            path += $"logs\\log{dataLogFormatada}.txt";

            //System.IO.Directory.CreateDirectory(path);

            using (var sw = new StreamWriter(path))
            {
                sw.WriteLine($"{msg} : {ex}");
            }

        }
    }
}
