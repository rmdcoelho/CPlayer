﻿using System.Collections.Generic;

namespace CPlayer.Entities
{
    public class Video
    {
        public int Index { get; set; }
        public string NameVideos { get; set; }
        public string FullPath { get; set; }
    }
}
