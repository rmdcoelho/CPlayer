﻿using CPlayer.Entities;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Threading;

namespace CPlayer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string fullPath;
        private double volume;
        private TimeSpan totalTime;
        private DispatcherTimer timerVideoTime;
        private int timeoutCarregamento;
        private int timeoutLimit;
        private string formatFullPath;
        private string actualVideo = string.Empty;
        private List<Video> Videos;
        private Log log = new Log();
        private bool isFullScreen = false;
        int videoIndex = 0;
        private Video selected;
        private double videoPosition;

        public MainWindow()
        {
            InitializeComponent();

            //Configuração da velocidade do vídeo:
            txtVelocidade.Text = "1.0";

            //Configuração do timeout de carregamento da duração do vídeo:
            timeoutCarregamento = 0;
            timeoutLimit = (int)1_000_000_000;

            //Configuração de volume:
            volume = 1;
            SldVolume.Value = 0.5;
            SldVolume.Maximum = volume;
            //Seta o volume:
            VideoControl.Volume = SldVolume.Value;

            //Configuração dos botões no início da aplicação:
            ButtonConfigurationInitialized();

            //Configuração da velocidade do vídeo:
            VideoControl.SpeedRatio = 1.0;

            //Lista de vídeo para adição do nome e path dos vídeo selecionados:
            Videos = new List<Video>();

            TimeLineSlider.AddHandler(MouseLeftButtonUpEvent,
                      new MouseButtonEventHandler(TimeLineSlider_MouseLeftButtonUp),
                      true);
        }

        private void BtnFolderPath_Click(object sender, RoutedEventArgs e)
        {
            PathList.Items.Clear();
            Videos.Clear();

            try
            {
                //Recupera o path do vídeo selecionado:
                var dialog = new FolderBrowserDialog();
                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    fullPath = dialog.SelectedPath;
                }

                formatFullPath = fullPath.Replace(" ", "");

                //Seta o caminho do vídeo no label
                txtfolderPath.Text = formatFullPath;
                txtfolderPath.IsEnabled = false;

                //video.FullPath = Directory.GetFiles(fullPath);
                foreach (var path in Directory.GetFiles(fullPath))
                {
                    videoIndex++;
                    Videos.Add(new Video() { Index = videoIndex, FullPath = path, NameVideos = Path.GetFileName(path) });
                }

                foreach (var video in Videos)
                {
                    PathList.Items.Add(video.NameVideos);
                }

                BtnPlay.IsEnabled = true;
            }
            catch (Exception ex)
            {
                log.WriteErros("Ocorreu o seguinte erro", ex);
            }
        }

        public void BtnFolderPreparation()
        {
            PathList.Items.Clear();
            Videos.Clear();

            txtfolderPath.IsEnabled = false;

            foreach (var path in Directory.GetFiles(txtfolderPath.Text))
            {
                videoIndex++;
                Videos.Add(new Video() { Index = videoIndex, FullPath = path, NameVideos = Path.GetFileName(path) });
            }

            foreach (var video in Videos)
            {
                PathList.Items.Add(video.NameVideos);
            }

            BtnPlay.IsEnabled = true;
        }

        private void BtnPlay_Click(object sender, RoutedEventArgs e)
        {
            if (txtfolderPath.Text.Length <= 0)
            {
                System.Windows.MessageBox.Show("Informe uma pasta válida");
                return;
            }

            if (PathList.SelectedItem == null)
            {
                System.Windows.MessageBox.Show("Selecione um vídeo");
                return;
            }

            var formato = Path.GetExtension(PathList.SelectedItem.ToString());

            if (!ValidaFormato(formato))
            {
                System.Windows.MessageBox.Show("Formato inválido");
                return;
            }

            InitializedVideo();

        }

        private void InitializedVideo()
        {
            var selectedName = (string)PathList.SelectedItem;
            selected = Videos.Where(x => x.NameVideos.Equals(selectedName)).FirstOrDefault();

            txtVideoName.Text = selected.NameVideos;

            if (VideoControl.Source != null && actualVideo.Equals(selected.FullPath))
            {
                try
                {
                    VideoControl.Play();
                    ButtonConfigurationPlay();
                }
                catch (Exception ex)
                {
                    log.WriteErros("Ocorreu o seguinte erro", ex);
                }
            }
            else
            {
                //actualVideo = (string)PathList.SelectedItem;
                actualVideo = selected.FullPath;

                //Insere o caminho do vídeo na configuração do player:
                //VideoControl.Source = new Uri(fullPath);
                VideoControl.Source = new Uri((string)selected.FullPath);

                try
                {
                    //Inicia o vídeo:
                    VideoControl.Play();

                    ButtonConfigurationPlay();

                    while (!VideoControl.NaturalDuration.HasTimeSpan)
                    {
                        //TimeoutCarregamento aumenta enquanto não haver HasTimeSpan e após
                        //Igualar com timeoutLimit ele sai do carregamento
                        timeoutCarregamento += 1;
                        if (timeoutCarregamento >= timeoutLimit)
                        {
                            System.Windows.MessageBox.Show("Vídeo não carregado");
                            return;
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.WriteErros("Ocorreu o seguinte erro", ex);
                }

                try
                {
                    //Guarda o valor de duração do vídeo:
                    totalTime = VideoControl.NaturalDuration.TimeSpan;
                    txtDuracao.Text = totalTime.ToString(@"hh\:mm\:ss");

                    //Seta no slider o valor, em segundos, da duração do vídeo:
                    //TimeLineSlider.Maximum = VideoControl.NaturalDuration.TimeSpan.TotalMilliseconds;
                    //TimeLineSlider.Maximum = 1;
                    TimeLineSlider.Maximum = totalTime.TotalSeconds;

                    timerVideoTime = new DispatcherTimer();
                    timerVideoTime.Interval = TimeSpan.FromSeconds(1);
                    timerVideoTime.Tick += new EventHandler(Timer_Tick);
                    timerVideoTime.Start();
                }
                catch (Exception ex)
                {
                    log.WriteErros("O seguinte erro ocorreu", ex);
                }
            }
        }

        private void BtnStop_Click(object sender, RoutedEventArgs e)
        {
            VideoControl.Stop();
            ButtonConfigurationInitialized();
            BtnPlay.IsEnabled = true;
        }

        private void BtnPause_Click(object sender, RoutedEventArgs e)
        {
            VideoControl.Pause();
            ButtonConfigurationInitialized();
            BtnPlay.IsEnabled = true;
        }

        private void SldVolume_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            VideoControl.Volume = SldVolume.Value;
        }

        private void Speed15_Click(object sender, RoutedEventArgs e)
        {
            VideoControl.SpeedRatio = 1.5;
            txtVelocidade.Text = "1.5";
        }

        private void Speed10_Click(object sender, RoutedEventArgs e)
        {
            VideoControl.SpeedRatio = 1.0;
            txtVelocidade.Text = "1.0";
        }

        private void Speed20_Click(object sender, RoutedEventArgs e)
        {
            VideoControl.SpeedRatio = 2.0;
            txtVelocidade.Text = "2.0";
        }

        private void Up_Click(object sender, RoutedEventArgs e)
        {
            VideoControl.Position += TimeSpan.FromSeconds(15);
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            VideoControl.Position -= TimeSpan.FromSeconds(15);
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            try
            {
                // Check if the movie finished calculate it's total time
                if (VideoControl.NaturalDuration.TimeSpan.TotalSeconds > 0)
                {
                    if (totalTime.TotalSeconds > 0)
                    {
                        // Updating time slider
                        //TimeLineSlider.Value = VideoControl.Position.TotalSeconds /
                        //                   totalTime.TotalSeconds;
                        TimeLineSlider.Value = VideoControl.Position.TotalSeconds;
                    }
                }
            }
            catch (Exception ex)
            {
                log.WriteErros("O seguinte erro ocorreu", ex);
            }

            txtPosicao.Text = VideoControl.Position.ToString(@"hh\:mm\:ss");
        }

        private void ButtonConfigurationInitialized()
        {
            BtnPlay.IsEnabled = false;
            BtnPause.IsEnabled = false;
            BtnStop.IsEnabled = false;

            Back.IsEnabled = false;
            Up.IsEnabled = false;

            Speed10.IsEnabled = false;
            Speed15.IsEnabled = false;
            Speed20.IsEnabled = false;
        }

        private void ButtonConfigurationPlay()
        {
            BtnPlay.IsEnabled = false;
            BtnPause.IsEnabled = true;
            BtnStop.IsEnabled = true;

            Back.IsEnabled = true;
            Up.IsEnabled = true;

            Speed10.IsEnabled = true;
            Speed15.IsEnabled = true;
            Speed20.IsEnabled = true;
        }

        private void Element_MediaEnded(object sender, EventArgs e)
        {
            VideoControl.Stop();
            ButtonConfigurationInitialized();
            BtnPlay.IsEnabled = true;

            //Permite que o próximo vídeo seja iniciado após o final do anterior:
            var index = selected.Index + 1;
            var videoSeguinte = Videos.Where(x => x.Index == index).FirstOrDefault();
            PathList.SelectedItem = (string)videoSeguinte.NameVideos;

            if(videoSeguinte != null)
            {
                VideoControl.Source = new Uri(videoSeguinte.FullPath);
                InitializedVideo();
            }
            else
            {
                System.Windows.MessageBox.Show("Playlist encerrada");
            }

            txtVideoName.Text = videoSeguinte.NameVideos;
            
        }

        private void PathList_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (PathList.SelectedItem == null)
            {
                return;
            }
            var selectedName = (string)PathList.SelectedItem;
            var selected = Videos.Where(x => x.NameVideos.Equals(selectedName)).FirstOrDefault();

            txtVideoName.Text = selected.NameVideos;
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            App.Current.MainWindow.Close();
        }

        //Atualiza a posição do vídeo através do slider:
        private void TimeLineSlider_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if(totalTime.TotalSeconds > 0)
            {
                VideoControl.Position = TimeSpan.FromSeconds(TimeLineSlider.Value);
            }
        }

        //Liga e desliga o fullscreen:
        private void MediaElement_MouseUp(object sender, EventArgs e)
        {
            if (!isFullScreen)
            {
                videoPosition = VideoControl.Position.TotalSeconds;
                layoutRoot.Children.Remove(VideoControl);
                this.Content = VideoControl;
                this.WindowStyle = WindowStyle.None;
                this.WindowState = WindowState.Maximized;
                VideoControl.Position = TimeSpan.FromSeconds(videoPosition);
            }
            else
            {
                videoPosition = VideoControl.Position.TotalSeconds;
                this.Content = CanvasContainer;
                layoutRoot.Children.Add(VideoControl);
                this.WindowStyle = WindowStyle.SingleBorderWindow;
                this.WindowState = WindowState.Normal;
                VideoControl.Position = TimeSpan.FromSeconds(videoPosition);
            }

            isFullScreen = !isFullScreen;
        }

        private bool ValidaFormato(string extensao)
        {
            if(extensao.Equals(".wmv") ||
                extensao.Equals(".avi") ||
                extensao.Equals(".mp4"))
            {
                return true;
            }
            return false;
        }

        private void Listar_Click(object sender, RoutedEventArgs e)
        {
            ListarPlaylist listarPlaylist = new ListarPlaylist();
            listarPlaylist.ShowDialog();
        }
    }
}
