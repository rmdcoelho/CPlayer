﻿using CPlayer.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CPlayer
{
    /// <summary>
    /// Interaction logic for ListarPlaylist.xaml
    /// </summary>
    public partial class ListarPlaylist : Window
    {
        private int videoIndex;
        private List<PlayList> lista;

        public ListarPlaylist()
        {
            InitializeComponent();

            lista = new List<PlayList>();


            var fullPath = @"C:\Users\Su\Desktop\DIREITO CONSTITUCIONAL";

            foreach (var path in Directory.GetDirectories(fullPath))
            {
                videoIndex++;
                lista.Add(new PlayList() { Index = videoIndex, FullPath = path });
            }

            foreach (var path in lista)
            {
                ListarPlaylists.Items.Add(path.FullPath);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).txtfolderPath.Text = ListarPlaylists.SelectedItem.ToString();
            ((MainWindow)Application.Current.MainWindow).BtnFolderPreparation();
            this.Close();
        }
    }
}
